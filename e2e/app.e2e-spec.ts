import { DeliveryAppPage } from './app.po';

describe('delivery-app App', function() {
  let page: DeliveryAppPage;

  beforeEach(() => {
    page = new DeliveryAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
