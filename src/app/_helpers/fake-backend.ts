import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

export let fakeBackendProvider = {
  provide: Http,
  useFactory: (backend: MockBackend, options: BaseRequestOptions) => {
    let users: any[] = JSON.parse(localStorage.getItem('users')) || [];

    backend.connections.subscribe((connection: MockConnection) => {
      console.log(connection);
      console.log(RequestMethod);
      setTimeout(() => {
        //authenticate
        if (connection.request.url.endsWith('/api/authenticate') && connection.request.method === RequestMethod.Post) {
          // get parameters from post request
          let params = JSON.parse(connection.request.getBody());

          //find if user matches login credentials
          let filteredUsers = users.filter(user => (user.username === params.username && user.password === params.password));

          if (filteredUsers.length) {
            //if user is found return status 200 with user details and JWT fake token
            let user = filteredUsers[0];
            connection.mockRespond(new Response(new ResponseOptions({
              status: 200,
              body: {
                id: user.id,
                username: user.username,
                firstName: user.firstName,
                lastName: user.lastName,
                token: 'fake-jwt-token'
              }
            })));
          }
          else {
            connection.mockError(new Error('User or password is incorrect'));
          }
        }
        //get users
        if (connection.request.url.endsWith('/api/users') && connection.request.method === RequestMethod.Get) {
          //check token in headers and return users if valid
          if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
            connection.mockRespond((new Response(new ResponseOptions({
              status: 200,
              body: users
            }))))
          }
          else {
            //return status 401 if token is invalid or null
            connection.mockRespond((new Response(new ResponseOptions({
              status: 401
            }))));
          }
        }
        //get user by id
        if (connection.request.url.match(/\/api\/users\/\d+$/) && connection.request.method === RequestMethod.Get) {
          // check token in headers and return one user if valid
          if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
            //find user by id in users array
            let urlParts = connection.request.url.split('/');
            let id = urlParts[urlParts.length - 1];
            let matchedUsers = users.filter(user => user.id === id);
            let user = matchedUsers.length ? matchedUsers[0] : null;
            connection.mockRespond((new Response(new ResponseOptions({
              status: 200,
              body: user
            }))));
          }
          else {
            //return status 401 if token is invalid or null
            connection.mockRespond((new Response(new ResponseOptions({
              status: 401,
            }))))
          }
        }
        //create new user
        if (connection.request.url.endsWith('/api/users') && connection.request.method === RequestMethod.Post) {
          //get new user object from post body
          let newUser = JSON.parse(connection.request.getBody());

          //check if the user exist in users
          let duplicateUsers = users.filter(user => user.username === newUser.username).length;
          if (!duplicateUsers) {
            newUser.id = users.length + 1;
            users.push(newUser);
            localStorage.setItem('users', JSON.stringify(users));
            connection.mockRespond(new Response(new ResponseOptions({
              status: 200
            })))
          }
          else {
            connection.mockError(new Error(`Username ${newUser.username} is already taken`));
          }
        }
        //delete user
        if (connection.request.url.match(/\/api\/users\/\d+$/) && connection.request.method === RequestMethod.Delete) {
          // check token in headers and return one user if valid
          if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
            //find user in an array by id
            let urlParts = connection.request.url.split('/');
            let id = urlParts[urlParts.length - 1];
            users.forEach((user, index) => {
              if (user.id === id) {
                users.splice(index, 1);
                localStorage.setItem('users', JSON.stringify(users));
              }
            });
            connection.mockRespond(new Response(new ResponseOptions({
              status: 200
            })));
          }
          else {
            //return 401 for not authenticated
            connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
          }
        }
      }, 1000);
    })
    return new Http(backend, options);
  },
  deps: [MockBackend, BaseRequestOptions]
}