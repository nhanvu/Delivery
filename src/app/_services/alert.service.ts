import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AlertService {
  private subject = new Subject<any>();
  constructor(private router: Router) {
    //clear alert message on route change
    router.events.subscribe(event => {
      
    })
  }
}