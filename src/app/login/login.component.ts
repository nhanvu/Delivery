import { Component, OnInit } from '@angular/core';

declare const FB: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  token: any;
  logged: boolean = false;
  user = { name: 'Hello'}

  constructor() {
    console.log('aaa');
    FB.init({
      appId      : '1208970832558353',
      cookie     : false, 
      xfbml      : false,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.5
    });
    FB.Canvas.setAutoGrow();
  }

  ngOnInit() {

  }

  onLogin() {
    FB.getLoginStatus(response => {
      this.statusChangeCallback(response);
    });
  }

  onLogout() {
    FB.logout(response => console.log(response));
  }

  statusChangeCallback(response: any) {
    console.log(response, 'response');
    if (response.status === 'connected') {
      console.log('connect');
    }
    else {
      FB.login(result => {
        console.log(result, 'result')
        this.logged = true;
        this.token = result.authResponse.accessToken;
        FB.api('/me?fields=id,name,first_name,gender,picture.width(150).height(150),age_range,friends', response => {
          console.log(response);
        })
      }, {scope: 'user_friends, email'});
    }
  }

  // login() {
  //   FB.login(result => {
  //     this.logged = true;
  //     this.token = result;
  //   },{scope: 'user_friends'})
  // }
}
