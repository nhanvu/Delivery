import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'

import { AlertService, UserService } from '../_services'

@Component({
  moduleId: module.id,
  templateUrl: 'register.component.html'
})

export class RegisterComponent {
  loading: boolean = false;
  model: any = {};

  constructor(
    private router: Router,
    private userService: UserService,
    private alertService: AlertService
  ) {}
  register() {
    this.loading = true;
    this.userService.create(this.model).subscribe(
      data => {
        this.alertService('Register successed', true);
        this.router.navigate(['/login']);
      },
      error => {
        this.alertService(error);
        this.loading = false;
      }
    )
  }
}